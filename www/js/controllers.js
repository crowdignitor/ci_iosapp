angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})



.controller('homeCtrl', function($scope) {})

/*.controller('companyCardsCtrl', function($scope , $http ){

        $scope.getCompanyDetails = function(){
             $http.get("http://192.168.16.163:3000/app/companyCards")
             .then(function(response) {
                if (status == 200) {
                console.log("<<<<<<<<<<");
                 
                $scope.cardId = data.companyHeading;
                $scope.companyName =data.companyName;
                    
                    console.log($scope.cardId );
                     console.log($scope.companyName);
             }
        });
};
});*/

angular.module('starter').controller('companyCardController', function($scope, $http,$state) {
    
     $scope.data = $state.params.data;
    
    console.log($state.params.data);
    
})