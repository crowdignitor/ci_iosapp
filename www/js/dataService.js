'use strict';

/*=================================================================
 *  * @SOURCE FILE
 *   *   $Id: datService.js
 *    *
 *     *
 *      * @REVISIONS
 *       =================================================================*/
angular.module('starter')
	.factory('dataService', function(localStorageService, $location) {

		var self = this;

		return {
			getCompanyHeading: function() {
				return localStorageService.get('companyHeading');
			},
			setCompanyHeading: function(companyHeading) {
				localStorageService.set('companyHeading', companyHeading);;
			},
			
			getCompanyName: function() {
				return localStorageService.get('companyName');
			},
			setCompanyName: function(roleName) {
				localStorageService.set('companyName', companyName);
			},
			
			getEndorsement: function() {
				return localStorageService.get('endorsement');
			},
			setEndorsement: function(endorsement) {
				localStorageService.set('endorsement', endorsement);
			},
			
			getReach: function() {
				return localStorageService.get('reach');
			},
			setReach: function(reach) {
				localStorageService.set('reach', reach);
			},
			
			getVideoUrl: function() {
				return videoUrl;
			},
			setVideoUrl: function(videoUrl) {
				videoUrl = videoUrl;
			},
			
			getFunding: function() {
				return localStorageService.get('funding');
			},
			setFunding: function(funding) {
				localStorageService.set('funding', funding);
			},
			
			getFoundingDate: function() {
			return localStorageService.get('foundingDate');
			},
			setFoundingDate: function(foundingDate) {
				localStorageService.set('foundingDate', foundingDate);
			},
			
			getDetails: function() {
			return localStorageService.get('details');
			},
			setDetails: function(details) {
				localStorageService.set('details', details);
			},
			
			getCompanyId: function() {
			return localStorageService.get('companyId');
			},
			setCompanyId: function(companyId) {
				localStorageService.set('companyId', companyId);
			}
		};
	});
